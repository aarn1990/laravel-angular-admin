<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    protected $guarded = [];

    protected $table ='cajas';

    protected $primaryKey = 'id';
    
    public function Agents()
    {
        return $this->belongsTo('App\Agent', 'agent_id');
    }
    public function tickets(){
        return $this->hasMany('App\tickets','tockets_id');
    }
    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
