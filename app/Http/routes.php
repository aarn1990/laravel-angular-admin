<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'AngularController@serveApp');
    Route::get('/unsupported-browser', 'AngularController@unsupported');
    Route::get('user/verify/{verificationCode}', ['uses' => 'Auth\AuthController@verifyUserEmail']);
    Route::get('auth/{provider}', ['uses' => 'Auth\AuthController@redirectToProvider']);
    Route::get('auth/{provider}/callback', ['uses' => 'Auth\AuthController@handleProviderCallback']);
    Route::get('/api/authenticate/user', 'Auth\AuthController@getAuthenticatedUser');
});

$api->group(['middleware' => ['api']], function ($api) {
     $api->post('auth/login', 'Auth\AuthController@postLogin');
     $api->get('auth/login', 'Auth\AuthController@postLogin');

    // Password Reset Routes...
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');
    
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->get('users/me', 'UserController@getMe');
    $api->get('allcaja', 'CajaController@allcaja');
    $api->get('alldist', 'CajaController@alldist');
    $api->get('gethour', 'AdminController@gethour');
    $api->put('users/me', 'UserController@putMe');
    $api->get('animalitos', 'AnimalitosController@index');
    $api->get('code', 'AnimalitosController@code');
    $api->resource('report', 'ReportesController@initialSearch');
    $api->resource('ticket/caje', 'TicketController@caje');
    $api->resource('ticket/win', 'TicketController@win');
    $api->resource('ticket/per', 'TicketController@per');
    $api->resource('ticket', 'TicketController');
    $api->resource('agent', 'AgentController');
    $api->resource('caja', 'CajaController');
    $api->resource('admin', 'AdminController');
    $api->resource('ganador', 'GanadorController');
    $api->post('configuration', 'ConfigurationController@update');
    $api->get('configuration/{id}', 'ConfigurationController@show');
    //$api->resource('configuration','ConfigurationController');
    $api->post('/auth/register','Auth\AuthController@postRegister');
});
$api->group(['middleware' => ['api']], function ($api) {
   
    $api->get('animalitos', 'AnimalitosController@index');
});

$api->group(['middleware' => ['api', 'api.auth', 'role:admin.super|admin.user']], function ($api) {
     $api->get('users', 'UserController@getIndex');
     $api->get('users/show/{id}', 'UserController@getShow');
     $api->put('users/show/{id}', 'UserController@putRolesShow');

     $api->get('users/find/{id}', 'UserController@getShow');
     $api->put('users/find', 'UserController@putUserState');

    $api->get('users/roles', 'UserController@getRoles');
    $api->get('users/permissions', 'UserController@getPermissions');
    $api->put('users/roles-show', 'UserController@putRolesShow');
    $api->get('users/roles-show/{id}', 'UserController@getRolesShow');
    $api->put('users/show', 'UserController@putShow');
});
