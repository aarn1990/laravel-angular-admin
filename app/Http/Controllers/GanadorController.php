<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ganador;
use Carbon\Carbon;
use App\tickets;

class GanadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();
        $carbom = Carbon::now();
        $carbom->setTimezone('America/Caracas');
        $date = $carbom->format("Y-m-d");
        $hour =(int)$carbom->format("H");
        $ganador = new ganador; 
        $ganador->date = $date;
        $ganador->hour = $hour;
        $ganador->animales_id = $req['id'];
        $ganador->pago = $req['ventas'] * $req['propocion'];
        $ganador->save();

        $tickets  = tickets::where("date",$date)->where("hour",$hour)->whereDate("created_at","<=",$date)->with("animales")->get();
        foreach ($tickets as $ticket) {
          foreach ($ticket->animales as $animal) {
              if($ganador->animales_id == $animal->id){
                $animal->pivot->ganador += 1;
                $animal->pivot->save();
                $ticket->state = 3;
                $ticket->save();
              }
          } 
        } 
        return response()->success("success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
