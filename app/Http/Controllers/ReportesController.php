<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Caja;
use App\Agent;
use App\Distribuidor;
use App\tickets;
use Carbon\Carbon;
use Auth;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function initialSearch(Request $request)
    {
        $req = $request->all();
        $caja = Caja::where('user_id',Auth::user()->id)->first();
        $agent = Agent::where('user_id',Auth::user()->id)->first();
        $dist = Distribuidor::where('user_id',Auth::user()->id)->first();
        $admin = 0;
        $test = "test";
        $result = [];
        $allresult = [];
        $inicialDate = $req['initialDate']=="" ? Carbon::now()->format("Y-m-d"):$req['initialDate'];
        $finalDate = $req['finalDate'] =="" ? $inicialDate:$req['finalDate'];
        $type = 3;
        if($caja == null && $agent == null && $dist == null) 
        {
            $admin = 1;
        }

        switch ($req['type']) {
            case 0:
                
                break;
            case 1:                
                $tickets = tickets::wherebetween('date',[$inicialDate,$finalDate]);
                if($admin == 1)
                {
                   
                    if($req['cajaid'] != 0){
                        $tickets->where('caja_id',$req['cajaid']);
                       
                    }
                    if($req['agentid'] != 0){
                       
                        $tickets->with(array('caja'=>function($query){
                            $query->where('agent_id',$req['agentid']);
                        }))->with('caja.Agents.user');
                    }
                    $type = 0;            
                                
                }elseif($caja) {
                    
                    $tickets->where('caja_id',$caja->id);
                    $type = 1;
                               
                }elseif($agent){
                    
                    $tickets->where('caja_id',$req['cajaid']);
                    $type = 2;
                                
                }
                $result =  $tickets->where('state','=','1')->with('animales','caja.user')->get();
                break;
            case 2:
            $ticketswin = tickets::wherebetween('date',[$inicialDate,$finalDate]);
            $ticketslos =tickets::wherebetween('date',[$inicialDate,$finalDate]);
            $ticketscan =tickets::wherebetween('date',[$inicialDate,$finalDate]);       
            if($admin == 1)
            {
                
                if($req['cajaid'] != 0){
                    $ticketswin->where('caja_id',$req['cajaid']);
                    $ticketslos->where('caja_id',$req['cajaid']);
                    $ticketscan->where('caja_id',$req['cajaid']);
                    
                }
                if($req['agentid'] != 0){
                    
                     $ticketswin->with(array('caja'=>function($query){
                         $query->where('agent_id',$req['agentid']);
                     }))->with('caja.Agents.user');
                     $ticketslos->with(array('caja'=>function($query){
                        $query->where('agent_id',$req['agentid']);
                    }))->with('caja.Agents.user');
                    $ticketscan->with(array('caja'=>function($query){
                        $query->where('agent_id',$req['agentid']);
                    }))->with('caja.Agents.user');
                 }
                $type = 0;            
                            
            }elseif($caja) {
                
                $ticketswin->where('caja_id',$caja->id);
                $ticketslos->where('caja_id',$caja->id);
                $ticketscan->where('caja_id',$caja->id);
                $type = 1;
                
                            
            }elseif($agent){
                
                $ticketswin->where('caja_id',$req['cajaid']);
                $ticketslos->where('caja_id',$req['cajaid']);
                $ticketscan->where('caja_id',$req['cajaid']);
                $type = 2;
                          
            }
          
            $allresult[1] =  $ticketswin->where('state','=','3')->with('animales','caja.user')->get();
            $allresult[2] =  $ticketslos->where('canjeado',0)->where('state','=','1')->with('animales','caja.user')->get();
            $allresult[3] =  $ticketscan->where('canjeado',1)->where('state','=','1')->with('animales','caja.user')->get();
            break;
            case 3:
            $tickets = tickets::wherebetween('date',[$inicialDate,$finalDate]);
            
            if($admin == 1)
            {
               
                if($req['cajaid'] != 0){
                    $tickets->where('caja_id',$req['cajaid']);
                    
                }
                if($req['agentid'] != 0){
                    
                     $tickets->with(array('caja'=>function($query){
                         $query->where('agent_id',$req['agentid']);
                     }))->with('caja.Agents.user');
                 }
                $type = 0;            
                            
            }elseif($caja) {
                
                $tickets->where('caja_id',$caja->id);
                $type = 1;
                           
            }elseif($agent){
                
                $type = 2;
                
                            
            }
           
            $result =  $tickets->where('state','=','2')->with('animales','caja.user')->get();
                break;
            case 4:
            $tickets = tickets::wherebetween('date',[$inicialDate,$finalDate]);
            
            if($admin == 1)
            {
               
                if($req['cajaid'] != 0){
                    $tickets->where('caja_id',$req['cajaid']);
                  
                    
                }
                if($req['agentid'] != 0){
                    
                     $tickets->with(array('caja'=>function($query){
                         $query->where('agent_id',$req['agentid']);
                     }))->with('caja.Agents.user');
                 }
                $type = 0;            
                $tickets->orderBy('caja_id','ASC');     
            }elseif($caja) {
                
                $tickets->where('caja_id',$caja->id);
                
                $type = 1;
                           
            }elseif($agent){
                
                $type = 2;
                $tickets->orderBy('caja_id','ASC');
            }
           
            $result =  $tickets->where('state','!=','2')->with('animales','caja.user')->get();
                break;
            case 5:
                 $test = $req['type'];
                break;
            case 6:
               $test = $req['type'];
                break;
            case 7:
                $test = $req['type'];
                break;
            case 8:
                 $test = $req['type'];
                break;
            case 9:
               $test = $req['type'];
                break;
            case 10:
                $test = $req['type'];
                break;
            case 11:
                 $test = $req['type'];
                break;
            case 12:
               $test = $req['type'];
                break;
            case 13:
                $test = $req['type'];
                break;
            case 14:
                 $test = $req['type'];
                break;
            case 15:
               $test = $req['type'];
                break;
            case 16:
                $test = $req['type'];
                break;
            case 17:
                 $test = $req['type'];
                break;
        }
       return response()->success(compact('result','type','allresult','ticketswin'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
