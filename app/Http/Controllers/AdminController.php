<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use \stdClass;
use App\tickets;
use Carbon\Carbon;
use App\Http\Requests;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $carbom = Carbon::now();
        $carbom->setTimezone('America/Caracas');
        $date = $carbom->format("Y-m-d");
        $hour =(int)$carbom->format("H");
        $finalHour = $hour;
        $min =(int)$carbom->format("m");
        if($min>=5){
            $finalHour += 1;
        }
        $admin = tickets::where("date",$date)->where("hour",$finalHour)->whereDate("created_at","<=",Carbon::today())->with("animales")->get();       
        return response()->success(compact('admin'));
    }
    public function gethour()
    {
        $gethour =[];
        $user = Auth::user();
        $carbom = Carbon::now();
        $carbom->setTimezone('America/Caracas');
        //$date = $carbom->format("Y-m-d");
        $gethour =(int)$carbom->format("H");
        $min =(int)$carbom->format("i");
       
        //$admin = tickets::where("date",$date)->where("hour",$hour+1)->whereDate("created_at","<=",Carbon::today())->with("animales")->get();       
        return response()->success(compact('gethour','min'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
