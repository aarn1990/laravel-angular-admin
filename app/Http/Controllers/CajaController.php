<?php

namespace App\Http\Controllers;

use App\Agent;
use Illuminate\Http\Request;

use Debugbar;
use App\User;
use Carbon\Carbon;
use DB;
use App\Caja;
use Auth;
use Hash;
use Input;
use Validator;
use App\Http\Requests;
use App\tickets;


class CajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $agent = DB::table('agents')->where('user_id',$user->id)->first();
        $caja = Caja::where('agent_id',$agent->id)->with("user")->get();
        return response()->success(compact('caja'));
    }
    public function allcaja()
    {
       
        $allcaja = Caja::with('user')->get();
        return response()->success(compact('allcaja'));
    }
    public function alldist()
    {
       
        $alldist = \App\Distribuidor::with('user')->get();
        return response()->success(compact('alldist'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
