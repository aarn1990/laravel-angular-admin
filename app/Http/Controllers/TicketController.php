<?php

namespace App\Http\Controllers;



use Debugbar;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use DB;
use App\Caja;
use Auth;
use Hash;
use Input;
use Validator;
use App\ganador;
use App\Http\Requests;
use App\tickets;



class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $caja = DB::table('cajas')->where('user_id',$user->id)->first();
        $ticket = tickets::where('caja_id',$caja->id)->where('state','=','1')->orderBy('code','desc')->get();
        return response()->success($ticket);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();
        $caja = Caja::where('user_id','=',$user->id);
        $caja = $user->caja()->get();
        $caja_id = $caja[0]->id;
        $req = new \stdClass();
        $req = $request->all();
        $rand = mt_rand();
        $tickets = \DB::table('tickets')->orderBy('id', 'desc')->first(); 
        $ticketsValue = 0;
        if($tickets != null){
            $ticketsValue = $tickets->id +1 ;
        }
        foreach ($req as $value)
        {
            
            $date = Carbon::parse($value['date']);
            $hour = $value['hour'];
            $code = $value['code'].$ticketsValue++;

            $tick = new tickets();
            $tick->caja_id = $caja_id;
            $tick->date = $date;
            $tick->hour = $hour;
            $tick->canjeado = false;

            $tick->code = $code;
            $tick->virtual_code = $rand;
            $tick->save();
            foreach ($value['animales'] as $animal)
            {
                $tick->animales()->attach($animal['id'],['value' => $animal['value'],'por_pagar'=>(int)$animal['value']*30]);
            }
        }

        return response()->success('susscess');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $caja = Caja::where('user_id',Auth::user()->id)->first();
        $ticketCode = tickets::where('code','=',$id)->where('state','=','1')->where('caja_id','=',$caja->id)->with(array('animales'=>function($query){
            $query->select('name','value');
        }))->first();
        //$date = Carbon::parse($ticketCode->created_at)->format("Y-m-d");
        //$ganador = ganador::where('date',$date)->where('hour',$ticketCode->hour)->get();
        //$ganador = DB::table('ganadors')->where('date',$date)->where('hour',$ticketCode->hour)->get();
        

        return response()->success($ticketCode);
    }
    public function caje(Request $request)
    {
        $req = $request->all();
        $json = json_decode($req['data'],true);
        $user = Auth::user();
        $ticket = tickets::find($json['id'])->first();

        if ($user["confirmationPass"] == $req['password']) {
            $ticket->canjeado = 1;
            $ticket->save();
        }else{
            return response()->success('error');
        }
        

        return response()->success('success');
    }
    public function win()
    {
       $ticket = tickets::where('canjeado',1)->where('state','=','1')->with('animales')->get();
        

        return response()->success($ticket);
    }
    public function per()
    {
       $ticket = tickets::where('state','=','1')->with('animales')->get();
        

        return response()->success($ticket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = tickets::find($id)->first();
        $min = Carbon::parse($ticket->create_at)->format('i');
        $minnow = Carbon::now()->format('i');
        $hournow = Carbon::now()->format('H');
        if($ticket->hour <= $hournow+1){
            if($min+10 >$minnow){
                $ticket->state = 2;
                $ticket->save();
            }
        }
        return response()->success($id);
    }
}
