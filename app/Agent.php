<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $guarded = [];
     public function Caja()
    {
        return $this->hasMany('App\Caja');
    }    
    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
