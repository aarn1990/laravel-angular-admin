<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tickets extends Model
{
    protected $guarded = [];

    protected $fillable = ['date','code','hour','caja_id'];

     public function animales()
    {
        return $this->belongsToMany('App\animales')->withPivot('Value','por_pagar','ganador');
    }
    public function Caja()
    {
        return $this->belongsTo('App\Caja');
    }
}
