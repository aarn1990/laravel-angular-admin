<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $guarded = [];
      public function Agent()
    {
        return $this->hasMany('App\Agent');
    }
}
