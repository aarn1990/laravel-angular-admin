class ReportController {
    constructor(AclService, ContextService, $scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $log, $document, $sce, $window) {
        'ngInject'

        let navSideBar = this
        this.can = AclService.can
        this.API = API;
        this.$scope = $scope;
        this.$state = $state;
        this.$window = $window;
        this.$log = $log;
        this.$document = $document;
        this.$compile = $compile;
        this.DTOptionsBuilder = DTOptionsBuilder;
        this.DTColumnBuilder = DTColumnBuilder;
        this.dtOption = [];
        this.dtColumn = [];
        this.agentlist = [];
        this.cajalist = [];
        this.distlist = [];
        this.caseone = [];
        this.totalAmount = 0;
        this.displayTable = [];
        this.type = 0;
        this.cajaId = 0;
        this.agentId = 0;
        this.distId = 0;
        this.initialDate = "";
        this.finalDate = "";
        this.report = this.API.service('report');
        let agent = this.API.service('agent');
        let caja = this.API.service('allcaja');
        let dist = this.API.service('alldist');
        agent.getList().then((response) => {
            var dataSet = response.plain();
            this.agentlist = dataSet;
        });
        caja.getList().then((response) => {
            var dataSet = response.plain();
            this.cajalist = dataSet;
        });
        dist.getList().then((response) => {
            var dataSet = response.plain();
            this.distlist = dataSet;
        });
        ContextService.me(function(data) {
            navSideBar.userData = data
        })
    }

    submit($event) {
        $event.stopPropagation();
        var req = {
            type: this.type,
            cajaid: this.cajaId,
            agentid: this.agentId,
            distId: this.distId,
            initialDate: this.initialDate,
            finalDate: this.finalDate
        }
        var me = this;
        this.report.one().get(req).then((reponse) => {

            var dataSet = reponse.plain();
            me.caseone = dataSet.data;
            me.totalAmount = 0;
            this.displayTable[0] = false;
            this.displayTable[1] = false;
            this.displayTable[2] = false;
            this.displayTable[3] = false;
            this.displayTable[4] = false;
            if (me.type == 1) {
                for (var j = 0; j < me.caseone.result.length; j++) {
                    for (var index = 0; index < me.caseone.result[j].animales.length; index++) {
                        me.totalAmount += me.caseone.result[j].animales[index].pivot.Value;
                    }
                }
                me.dtOptions = me.DTOptionsBuilder.newOptions()
                    .withOption('data', me.caseone.result)
                    //.withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()
                if (dataSet.data.type == 1) {

                    me.dtColumns = [
                        me.DTColumnBuilder.newColumn('id').withTitle('id'),
                        me.DTColumnBuilder.newColumn('date').withTitle('fecha de jugada'),
                        me.DTColumnBuilder.newColumn('hour').withTitle('hora de jugada'),
                        me.DTColumnBuilder.newColumn('created_at').withTitle('fecha de emision'),
                        me.DTColumnBuilder.newColumn('animales').withTitle('Valor Total').renderWith(function(data) {
                            var result = 0;
                            for (var i = 0; i < data.length; i++) {
                                result += data[i].pivot.Value;
                            }
                            return result;
                        })
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)
                    ]



                } else {

                    me.dtColumns = [
                        me.DTColumnBuilder.newColumn('id').withTitle('id'),
                        me.DTColumnBuilder.newColumn('caja.user.username').withTitle('username'),
                        me.DTColumnBuilder.newColumn('caja.agents.user.username').withTitle('agent username'),
                        me.DTColumnBuilder.newColumn('date').withTitle('fecha de jugada'),
                        me.DTColumnBuilder.newColumn('hour').withTitle('hora de jugada'),
                        me.DTColumnBuilder.newColumn('created_at').withTitle('fecha de emision'),
                        me.DTColumnBuilder.newColumn('animales').withTitle('Valor Total').renderWith(function(data) {
                            var result = 0;
                            for (var index = 0; index < data.length; index++) {
                                result += data[index].pivot.Value;
                            }
                            return result;
                        })
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)
                    ]


                }
                this.displayTable[0] = true;
            } else if (me.type == 2) {
                for (var k = 1; k < 4; k++) {

                    me.dtOption[k] = me.DTOptionsBuilder.newOptions()
                        .withOption('data', me.caseone.allresult[k])
                        //.withOption('createdRow', createdRow)
                        .withOption('responsive', true)
                        .withBootstrap()
                    me.dtColumn[k] = [
                        me.DTColumnBuilder.newColumn('id').withTitle('id'),
                        me.DTColumnBuilder.newColumn('caja.user.username').withTitle('username'),
                        me.DTColumnBuilder.newColumn('caja.agents.user.username').withTitle('agent username'),
                        me.DTColumnBuilder.newColumn('date').withTitle('fecha de jugada'),
                        me.DTColumnBuilder.newColumn('hour').withTitle('hora de jugada'),
                        me.DTColumnBuilder.newColumn('created_at').withTitle('fecha de emision'),
                        me.DTColumnBuilder.newColumn('animales').withTitle('Valor Total').renderWith(function(data) {
                            var result = 0;
                            for (var index = 0; index < data.length; index++) {
                                result += data[index].pivot.Value;
                            }
                            return result;
                        })
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)
                    ]
                    this.displayTable[k] = true;
                }
            } else if (me.type == 3) {
                me.dtOptions = me.DTOptionsBuilder.newOptions()
                    .withOption('data', me.caseone.result)
                    //.withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()
                if (dataSet.data.type == 1) {

                    me.dtColumns = [
                        me.DTColumnBuilder.newColumn('id').withTitle('id'),
                        me.DTColumnBuilder.newColumn('date').withTitle('fecha de jugada'),
                        me.DTColumnBuilder.newColumn('hour').withTitle('hora de jugada'),
                        me.DTColumnBuilder.newColumn('created_at').withTitle('fecha de emision'),
                        me.DTColumnBuilder.newColumn('animales').withTitle('Valor Total').renderWith(function(data) {
                            var result = 0;
                            for (var i = 0; i < data.length; i++) {
                                result += data[i].pivot.Value;
                            }
                            return result;
                        })
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)
                    ]
                    this.displayTable[4] = true;


                } else {

                    me.dtColumns = [
                        me.DTColumnBuilder.newColumn('id').withTitle('id'),
                        me.DTColumnBuilder.newColumn('caja.user.username').withTitle('caja username'),
                        me.DTColumnBuilder.newColumn('caja.agents.user.username').withTitle('agent username'),
                        me.DTColumnBuilder.newColumn('date').withTitle('fecha de jugada'),
                        me.DTColumnBuilder.newColumn('hour').withTitle('hora de jugada'),
                        me.DTColumnBuilder.newColumn('created_at').withTitle('fecha de emision'),
                        me.DTColumnBuilder.newColumn('animales').withTitle('Valor Total').renderWith(function(data) {
                            var result = 0;
                            for (var index = 0; index < data.length; index++) {
                                result += data[index].pivot.Value;
                            }
                            return result;
                        })
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)

                    ]
                    this.displayTable[4] = true;

                }
            } else if (me.type == 4) {
                var balance = [];
                var entrada = 0;
                var salida = 0;
                var caja = [];
                var id = 1;
                var tempíd = 1;
                if (dataSet.data.type == 1) {
                    for (var j = 0; j < me.caseone.result.length; j++) {
                        if (me.caseone.result[j].state == 1) {
                            for (var index = 0; index < me.caseone.result[j].animales.length; index++) {
                                entrada += me.caseone.result[j].animales[index].pivot.Value;
                            }
                        } else {
                            for (var h = 0; h < me.caseone.result[j].animales.length; h++) {
                                if (me.caseone.result[j].animales[h].pivot.ganador != 0) {
                                    salida += me.caseone.result[j].animales[h].pivot.Value * 30;
                                } else {
                                    entrada += me.caseone.result[j].animales[h].pivot.Value;
                                }

                            }
                        }

                    }
                    balance.push({

                        entrada: entrada,
                        salida: salida,
                        total: entrada - salida
                    })


                } else {
                    for (var j = 0; j < me.caseone.result.length; j++) {
                        if (me.caseone.result[j].caja_id == id) {
                            balance.push({
                                //username: me.caseone.result[j - 1].caja.user.username,
                                entrada: entrada,
                                salida: salida,
                                total: entrada - salida
                            })
                            id++;
                            entrada = salida = 0;
                        }
                        if (me.caseone.result[j].state == 1) {
                            for (var index = 0; index < me.caseone.result[j].animales.length; index++) {
                                entrada += me.caseone.result[j].animales[index].pivot.Value;
                            }
                        } else {
                            for (var h = 0; h < me.caseone.result[j].animales.length; h++) {
                                if (me.caseone.result[j].animales[h].pivot.ganador != 0) {
                                    salida += me.caseone.result[j].animales[h].pivot.Value * 30;
                                } else {
                                    entrada += me.caseone.result[j].animales[h].pivot.Value;
                                }

                            }
                        }

                    }

                }



                me.dtOptions = me.DTOptionsBuilder.newOptions()
                    .withOption('data', balance)
                    //.withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()
                if (dataSet.data.type == 1) {

                    me.dtColumns = [

                        me.DTColumnBuilder.newColumn('entrada').withTitle('ticket perdido'),
                        me.DTColumnBuilder.newColumn('salida').withTitle('ticket ganado'),
                        me.DTColumnBuilder.newColumn('total').withTitle('balance')
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)
                    ]
                    this.displayTable[5] = true;


                } else {

                    me.dtColumns = [
                        //me.DTColumnBuilder.newColumn('username').withTitle('caja username'),
                        me.DTColumnBuilder.newColumn('entrada').withTitle('ticket perdido'),
                        me.DTColumnBuilder.newColumn('salida').withTitle('ticket ganado'),
                        me.DTColumnBuilder.newColumn('total').withTitle('balance')
                        // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        //.renderWith(actionsHtml)

                    ]
                    this.displayTable[5] = true;

                }
            }

        })

    }
}

export const ReportComponent = {
    templateUrl: './views/app/components/report/report.component.html',
    controller: ReportController,
    controllerAs: 'vm',
    bindings: {}
};