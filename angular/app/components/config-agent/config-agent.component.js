class ConfigAgentController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const ConfigAgentComponent = {
    templateUrl: './views/app/components/config-agent/config-agent.component.html',
    controller: ConfigAgentController,
    controllerAs: 'vm',
    bindings: {}
};
