class AdminHomeController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $log, $document, $sce, $window) {
        'ngInject';
        this.API = API;
        this.$scope = $scope;
        this.$state = $state;
        this.$window = $window;
        this.$log = $log;
        this.$document = $document;
        this.$compile = $compile;
        let admin = this.API.service('admin');
        let animal = this.API.service('animalitos');
        let config = this.API.service('configuration');
        let hour = this.API.service('gethour');
        let ticket = this.API.service('ticket');
        this.gan = this.API.service('ganador');
        this.per = this.API.service('perdedor');
        this.configurations = [];
        this.pago = [];
        this.totalPagado = 0;
        this.totalCobrado = 0;
        this.totalRecolectado = 0;
        this.animales = [];
        this.admins = [];
        this.ventas = [];
        this.disabled = [];
        this.hours = [];
        animal.getList()
            .then((response) => {
                let dataSet = response.plain()
                this.animales = dataSet;
                for (var index = 0; index < this.animales.length; index++) {
                    this.ventas[index + 1] = 0;
                    this.disabled[index + 1] = true;
                    this.pago[index + 1] = 0;
                }


            });
        admin.getList()
            .then((response) => {
                let dataSet = response.plain()
                this.admins = dataSet;
                for (var i = 0; i < this.admins.length; i++) {
                    var element = this.admins[i];
                    for (var index = 0; index < element.animales.length; index++) {
                        this.ventas[element.animales[index].pivot["animales_id"]] += element.animales[index].pivot["Value"];
                        this.totalRecolectado += element.animales[index].pivot["Value"];

                    }

                }

            });
        config.one(1).get()
            .then((response) => {
                let dataSet = response.plain()
                this.configurations = dataSet.data.configuration;
                hour.one().get()
                    .then((response) => {
                        let dataSet = response.plain()

                        var len = this.configurations[0].final_our - this.configurations[0].inicial_hour;

                        for (var i = 0; i < len; i++) {
                            if (dataSet.data.gethour == this.configurations[0].inicial_hour + i && dataSet.data.min <= 5) {
                                for (var index = 0; index < this.animales.length; index++) {
                                    this.disabled[index + 1] = false;
                                }
                            }

                        }
                        //this.configurations = dataSet.data.configuration;
                    });
                ticket.one("win").get().then((response) => {
                    let dataSet = response.plain()

                    for (var index = 0; index < dataSet.data.length; index++) {
                        var animales = dataSet.data[index].animales;
                        for (var i = 0; i < animales.length; i++) {
                            if (animales[i].pivot.ganador == 1) {
                                this.pago[animales[i].id] += animales[i].pivot.Value;
                                this.totalPagado += animales[i].pivot.Value;
                            }

                        }

                    }
                });
                ticket.one("per").get().then((response) => {
                    let dataSet = response.plain()

                    for (var index = 0; index < dataSet.data.length; index++) {
                        var animales = dataSet.data[index].animales;
                        for (var i = 0; i < animales.length; i++) {

                            //this.pago[animales[i].id] += animales[i].pivot.Value;
                            this.totalCobrado += animales[i].pivot.Value;


                        }

                    }
                });
            });



        //
    }

    $onInit() {}

    ganador(animalId) {

        var data = {
            ventas: this.ventas[animalId],
            id: animalId,
            propocion: this.configurations[0].proporcion
        }
        this.gan.post(data).then((response) => {
            if (response.data == 'success') {
                swal({
                    title: 'success'
                })
            }


        })
    }
}

export const AdminHomeComponent = {
    templateUrl: './views/app/components/admin-home/admin-home.component.html',
    controller: AdminHomeController,
    controllerAs: 'vm',
    bindings: {}
};