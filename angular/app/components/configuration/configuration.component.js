class ConfigurationController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $log) {
        'ngInject';

        this.API = API
        this.$state = $state
        this.$log = $log

        this.configurations = [];
        this.config = this.API.service('configuration');


        this.proporcion = 0;
        this.inicial = 0;
        this.final = 0;
        this.minima = 0;
        this.open = true;
        this.credito = 0;
        this.max_tickets_dia_eli = 0;


        this.config.one(1).get()
            .then((response) => {
                let dataSet = response.plain()
                var configurations = dataSet.data.configuration;
                this.proporcion = configurations[0].proporcion;
                this.inicial = configurations[0].inicial_hour;
                this.final = configurations[0].final_our;
                this.minimo = configurations[0].min_apus;
                this.open = configurations[0].open == 1 ? true : false;
                this.credito = configurations[0].credito;
                this.max_tickets_dia_eli = configurations[0].max_tickets_dia_eli;
                this.role = API.copy(response);
            });
    }
    save(isValid) {
        if (isValid) {
            var configuration = {
                proporcion: this.proporcion,
                inicial: this.inicial,
                final: this.final,
                minimo: this.minimo,
                open: this.open,
                credito: this.credito,
                max_tickets_dia_eli: this.max_tickets_dia_eli
            }
            var me = this;
            this.config.post(configuration, 1)
                .then((response) => {
                    if (response.data == 'success') {
                        swal({
                            title: 'guardado exitosamente'
                        })
                        let dataSet = response.plain()
                        var configurations = dataSet.data.configuration;
                        this.proporcion = configurations[0].proporcion;
                        this.inicial = configurations[0].inicial_hour;
                        this.final = configurations[0].final_our;
                        this.minimo = configurations[0].min_apus;
                        this.open = configurations[0].open == 1 ? true : false;
                        this.credito = configurations[0].credito;
                        this.max_tickets_dia_eli = configurations[0].max_tickets_dia_eli;
                    }
                })
        }

    }

    $onInit() {}
}

export const ConfigurationComponent = {
    templateUrl: './views/app/components/configuration/configuration.component.html',
    controller: ConfigurationController,
    controllerAs: 'vm',
    bindings: {}
};