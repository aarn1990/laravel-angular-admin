class AdminHomeController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const AdminHomeComponent = {
    templateUrl: './views/app/components/AdminHome/AdminHome.component.html',
    controller: AdminHomeController,
    controllerAs: 'vm',
    bindings: {}
};
