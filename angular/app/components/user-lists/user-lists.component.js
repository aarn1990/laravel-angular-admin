class UserListsController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
        'ngInject'
        this.API = API
        this.$state = $state
        this.checked = [];
        let Users = this.API.service('users')

        Users.getList()
            .then((response) => {
                let dataSet = response.plain()
                for (var index = 0; index < dataSet.length; index++) {
                    this.checked[index + 1] = dataSet[index].status == 1 ? true : false;

                }
                this.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('data', dataSet)
                    .withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()

                this.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('name').withTitle('Name'),
                    DTColumnBuilder.newColumn('email').withTitle('Email'),
                    DTColumnBuilder.newColumn('username').withTitle('username'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml)
                ]

                this.displayTable = true
            })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
            return `
                <a class="btn btn-xs btn-warning" ui-sref="app.useredit({userId: ${data.id}})">
                    <i class="fa fa-edit"></i>
                </a>
                &nbsp
                <label class="switch">
                    <input type="checkbox" ng-model="vm.checked[${data.id}]" ng-click="vm.block(${data.id})">
                    <span class="slider"></span>
                </label>`
        }
    }

    block(userId) {
        let API = this.API
        let $state = this.$state

        API.one('users').one('find', userId).get()
            .then((response) => {
                //var user = response;
                response.status = this.checked[userId];
                response.put();
                //let dataSet = response.plain()
                /* var post = {
                         id: dataSet.id,
                         status: dataSet.status
                 }*/
                //$state.reload()
                /* API.one('users').one('find', userId).put(post).then((response) => {
                     swal(response);
                     //$state.reload();
                 })*/
            })

    }

    $onInit() {}
}

export const UserListsComponent = {
    templateUrl: './views/app/components/user-lists/user-lists.component.html',
    controller: UserListsController,
    controllerAs: 'vm',
    bindings: {}
}