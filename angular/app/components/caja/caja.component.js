class CajaController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $log, $document, $sce, $window) {
        'ngInject';

        this.API = API;
        this.$scope = $scope;
        this.$state = $state;
        this.$window = $window;
        this.$log = $log;
        this.isActive = false;
        this.$document = $document;
        let animal = this.API.service('animalitos');
        let codes = this.API.service('code');
        this.tick = this.API.service('ticket');

        this.$compile = $compile;
        this.htmlString = "";
        this.value = [];
        this.total = 0;
        this.animales = [];
        this.code = [];
        this.checked = [];
        this.ticket = [];
        this.$sce = $sce;

        this.breakpointAccess = false;
        animal.getList()
            .then((response) => {
                let dataSet = response.plain()
                this.animales = dataSet;


            });
        codes.getList()
            .then((response) => {
                let dataSet = response.plain()
                this.code = dataSet;


            });

    }

    $onInit() {
        let today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '-' + mm + '-' + yyyy;
        this.initialDate = today;
    }
    check($event) {
        var a = angular.element(window.document.getElementsByClassName("inputs"));
        angular.element(window.document.getElementsByClassName("ticket-body")).remove();
        var me = this;
        if (!angular.element($event.currentTarget).hasClass('toggle-cheked')) {

            angular.element($event.currentTarget).addClass('toggle-cheked');

            a.prepend('<input required type="text" class="animal form-control ' + angular.element($event.currentTarget).next()[0].name + '" ng-model="vm.value[' + angular.element($event.currentTarget).next()[0].id + ']" placeholder="' + angular.element($event.currentTarget).next()[0].name + '" name="' + angular.element($event.currentTarget).next()[0].name + '" id="' + angular.element($event.currentTarget).next()[0].id + '">');
            a.find('.' + angular.element($event.currentTarget).next()[0].name).on('keydown', function(e) {
                me.$scope.$apply(function() {

                    if (e.which == 13) {
                        //location.path('/search/');
                        event.preventDefault();
                        angular.element('.' + angular.element($event.currentTarget).next()[0].name).next().focus();
                        me.suma();
                        me.$window.stop(); // Works in all browsers but IE    
                        window.document.execCommand("Stop"); // Works in IE
                        return false;

                    }
                });
            }).focus();

            this.$compile(a)(this.$scope);

        } else {
            angular.element($event.currentTarget).removeClass('toggle-cheked');
            a.find('.' + angular.element($event.currentTarget).next()[0].name).remove();
            this.value.splice(angular.element($event.currentTarget).next()[0].id, 1);
            me.suma();

        }

        $event.stopPropagation();
    }

    submit($event) {
        this.suma();
        var a = angular.element(window.document.getElementsByClassName("ticket"));
        var animal = window.document.getElementsByClassName("animal");
        var me = this;
        $event.stopPropagation();
        if (this.checked.length > 0 && animal.length > 0) {
            angular.forEach(this.checked, function(value, key) {

                var allHour = window.document.getElementsByClassName("hour");
                var hour = angular.element(allHour[key]);
                var event = {};
                var animalTicket = [];
                var animalstring = "";
                var fechaString = "";
                var hourString = "";
                var divTop = "";
                for (var i = 0; i < animal.length; i++) {

                    var anu = angular.element(animal[i]);

                    event.id = anu[0].id;
                    event.name = anu[0].name;
                    event.value = anu[0].value;
                    me.$log.log(i, animal.length);
                    animalTicket.push({
                        id: anu[0].id,
                        name: anu[0].name,
                        value: anu[0].value
                    });
                    animalstring += '<div><span tyle="margin-right: 15px">' + event.name + '  </span> : <span> ' + event.value + '</span></div>';

                    //event = {};
                    me.$log.log(animalTicket);
                }
                //event = {};
                event.date = me.initialDate;
                fechaString += '<span style="margin-right: 15px">' + event.date + '</span>'
                event.hour = hour[0].innerText;
                hourString += '<span>' + event.hour + '</span>';
                event.animales = animalTicket;

                event.code = event.date.replace(/-/g, "");
                divTop += '<div>' + fechaString + hourString + '</div>';
                me.ticket.push(event);
                a.append('<div class="ticket-body" style="border:solid 1px black">' +
                    divTop +
                    animalstring +
                    '</div>');

                me.$compile(a)(me.$scope);

            });
        } else {
            swal({
                title: 'ERROR',
                text: 'Selecione al menos un horario un animal o ambos'
            });
        }

    }
    imprimir() {
        var me = this;

        this.tick.post(me.ticket).then((response) => {
            let dataSet = response.plain();

            if (dataSet.data == 'susscess') {
                swal("Exelente!", "Imprimiendo", "success");
                me.limpiar();
                me.$log.log("hola")
            }
        });
    }
    suma() {
        this.total = 0;
        var me = this;
        angular.forEach(this.value, function(value) {
            if (value == angular.isUndefined) {
                me.breakpointAccess = true;

            }
            if (me.breakpointAccess) {
                me.$window.alert("ingrese un valor en las casillas de entrada");
                me.total = 0;
            } else
                me.total += parseInt(value);
        })
        this.$log.log(me.checked, me.checked.length);
        me.total *= me.checked.filter(function(value) { return value !== undefined }).length;
    }
    limpiar() {

        for (var index = 0; index < this.checked.length; index++) {
            this.checked[index] = false;

        }
        angular.element(window.document.getElementsByClassName("animal")).remove();
        angular.element(window.document.getElementsByClassName("ticket-body")).remove();
        angular.element(window.document.getElementsByClassName("toggle-cheked")).removeClass("toggle-cheked")
        this.value = [];
        this.total = 0;
        this.code = [];
        this.checked = [];
        this.ticket = [];
        this.suma();
    }





}

export const CajaComponent = {
    templateUrl: './views/app/components/caja/caja.component.html',
    controller: CajaController,
    controllerAs: 'vm',
    bindings: {}

}