class AgentListController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
        'ngInject'
        this.API = API
        this.$state = $state

        let agent = this.API.service('agent')

        agent.getList()
            .then((response) => {
                let dataSet = response.plain()

                this.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('data', dataSet)
                    .withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()

                this.dtColumns = [
                    DTColumnBuilder.newColumn('user.id').withTitle('ID'),
                    DTColumnBuilder.newColumn('user.name').withTitle('Name'),
                    DTColumnBuilder.newColumn('user.email').withTitle('email'),
                    DTColumnBuilder.newColumn('user.username').withTitle('username'),
                    DTColumnBuilder.newColumn('proporcion').withTitle('proporcion').renderWith(function(data) {
                        return data + '%';
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml)
                ]
                this.displayTable = true
            })
        let actionsHtml = (data) => {
            return `
                    <a class="btn btn-xs btn-warning" ui-sref="app.configagent({agentid: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>`
        }
        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

    }

    delete(roleId) {
        let API = this.API
        let $state = this.$state

        swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this data!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: false
        }, function() {
            API.one('users').one('roles', roleId).remove()
                .then(() => {
                    swal({
                        title: 'Deleted!',
                        text: 'User Role has been deleted.',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function() {
                        $state.reload()
                    })
                })
        })
    }

    $onInit() {}
}

export const AgentListComponent = {
    templateUrl: './views/app/components/agent-list/agent-list.component.html',
    controller: AgentListController,
    controllerAs: 'vm',
    bindings: {}
};