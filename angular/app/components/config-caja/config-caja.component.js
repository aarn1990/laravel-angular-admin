class ConfigCajaController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const ConfigCajaComponent = {
    templateUrl: './views/app/components/config-caja/config-caja.component.html',
    controller: ConfigCajaController,
    controllerAs: 'vm',
    bindings: {}
};
