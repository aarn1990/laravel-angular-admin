class AddAgentController {
    constructor($auth, $state, $scope, $stateParams, API) {
        'ngInject';

        //
        this.$auth = $auth
        this.$state = $state
        this.$scope = $scope

        this.password = ''
        this.password_confirmation = ''
        this.formSubmitted = false
        this.errors = []

        this.agentId = $stateParams.agentid == angular.undefined ? 0 : $stateParams.agentid;
        let UserData = API.service('agent');
        if (this.agentId != 0) {
            UserData.one(this.agentId).get()
                .then((response) => {
                    let dataSet = response.plain();
                    this.name = dataSet.data.agent.user.name;
                    this.email = dataSet.data.agent.user.email;
                    this.userid = dataSet.data.agent.user.id;
                    this.username = dataSet.data.agent.user.username;
                    this.participacion = dataSet.data.agent.proporcion
                })
        }
    }

    $onInit() {
        this.name = ''
        this.email = ''
        this.password = ''
        this.password_confirmation = ''
        this.username = ''
        this.participacion = 0
        this.userid = 0
    }
    register(isValid) {
        if (isValid) {
            var user = {
                name: this.name,
                email: this.email,
                password: this.password,
                password_confirmation: this.password_confirmation,
                username: this.username,
                participacion: this.participacion,
                caja: false,
                agentId: this.userid
            }

            this.$auth.signup(user)
                .then(() => {
                    this.$state.go('app.agentlist', { registerSuccess: true })
                })
                .catch(this.failedRegistration.bind(this))
        } else {
            this.formSubmitted = true
        }
    }

    failedRegistration(response) {
        if (response.status === 422) {
            for (var error in response.data.errors) {
                this.errors[error] = response.data.errors[error][0]
                this.$scope.userForm[error].$invalid = true
            }
        }
    }
}

export const AddAgentComponent = {
    templateUrl: './views/app/components/add-agent/add-agent.component.html',
    controller: AddAgentController,
    controllerAs: 'vm',
    bindings: {}
};