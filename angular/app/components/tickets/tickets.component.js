class TicketsController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $log, $document, $sce, $window) {
        'ngInject';

        this.API = API;
        this.$scope = $scope;
        this.$state = $state;
        this.$log = $log;
        this.$window = $window;
        this.disabled = true;
        this.ganador = 0;
        this.input = "";

        this.ticket = this.API.service('ticket');
        this.caje = this.API.service('ticket');

        this.ticket.one().get().then((response) => {

            let dataSet = response.plain();


            this.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('data', dataSet.data)
                .withOption('createdRow', createdRow)
                .withOption('responsive', true)
                .withBootstrap()

            this.dtColumns = [
                DTColumnBuilder.newColumn('code').withTitle('Codigo'),
                DTColumnBuilder.newColumn('date').withTitle('Fecha'),
                DTColumnBuilder.newColumn('created_at').withTitle('Creado'),
                DTColumnBuilder.newColumn('hour').withTitle('Hora de sorteo').renderWith(function(data, a, i) {
                    return data + ':00';
                }),
                DTColumnBuilder.newColumn('canjeado').withTitle('Canjeado').renderWith(function(data, a, i) {
                    return data == 1 ? 'Canjeado' : 'No Canjeado';
                }),
                DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                .renderWith(actionsHtml)
                // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()

            ]

            this.displayTable = true
        });

        this.mainTicket = [];
        let actionsHtml = (data) => {
            return `
                      
                      <button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                          <i class="fa fa-trash-o"></i>
                      </button>`
        }
        let createdRow = (row) => {
                $compile(angular.element(row).contents())($scope)
            }
            //
    }


    $onInit() {}
    codeSearch(code) {
        //let userId = this.$stateParams.userId;
        let me = this;
        this.ticket.one(code).get()
            .then((response) => {
                let dataSet = response.plain();
                me.mainTicket = dataSet;

                let len = me.mainTicket.data.animales;
                for (var index = 0; index < len.length; index++) {
                    if (len[index].pivot.ganador == 1) {
                        me.ganador = 1;
                        me.disabled = false;
                    }
                }
            })

    }
    canjear() {
        var me = this;
        swal({
                title: "Canje",
                text: "introdusca el codigo virtual:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Codigo Virtual"
            },
            function(inputValue) {
                if (inputValue === false) return false;

                if (inputValue === "") {
                    swal.showInputError("pescribe algo");
                    return false
                }
                me.mainTicket['password'] = inputValue;
                me.caje.one("caje").get(me.mainTicket).then((response) => {
                    if (response.data == 'success') {
                        swal("Nice!", "Canje completado exitosamente");
                        me.$state.reload();
                    }

                });

            });
    }
    delete(ticketId) {

        let me = this;
        let $state = this.$state
        this.$log.log("enter");
        swal({
            title: 'stas seguro?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Borrar',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: false
        }, function() {
            me.API.one('ticket', ticketId).remove()
                .then(() => {
                    swal({
                        title: 'Borrado!',
                        closeOnConfirm: true
                    }, function() {
                        $state.reload()
                    })
                })
        })
    }
}

export const TicketsComponent = {
    templateUrl: './views/app/components/tickets/tickets.component.html',
    controller: TicketsController,
    controllerAs: 'vm',
    bindings: {}
};