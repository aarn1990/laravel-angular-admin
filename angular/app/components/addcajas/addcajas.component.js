class AddcajasController {
    constructor($auth, $state, $scope) {
        'ngInject';

        //
        this.$auth = $auth
        this.$state = $state
        this.$scope = $scope

        this.password = ''
        this.password_confirmation = ''
        this.formSubmitted = false
        this.errors = []
        this.passwordConfig = ''
    }

    $onInit() {
        this.name = ''
        this.email = ''
        this.password = ''
        this.password_confirmation = ''
        this.username = ''
        this.passwordConfig = '';
        this.participacion = 0
    }
    register(isValid) {
        if (isValid) {
            var user = {
                name: this.name,
                email: this.email,
                password: this.password,
                password_confirmation: this.password_confirmation,
                username: this.username,
                participacion: this.participacion,
                passwordConfig: this.passwordConfig,
                caja: true

            }

            this.$auth.signup(user)
                .then(() => {
                    this.$state.go('app.listcaja', { registerSuccess: true })
                })
                .catch(this.failedRegistration.bind(this))
        } else {
            this.formSubmitted = true
        }
    }

    failedRegistration(response) {
        if (response.status === 422) {
            for (var error in response.data.errors) {
                this.errors[error] = response.data.errors[error][0]
                this.$scope.userForm[error].$invalid = true
            }
        }
    }

}

export const AddcajasComponent = {
    templateUrl: './views/app/components/addcajas/addcajas.component.html',
    controller: AddcajasController,
    controllerAs: 'vm',
    bindings: {}
};