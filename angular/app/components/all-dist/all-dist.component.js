class AllDistController {
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
        'ngInject'
        this.API = API
        this.$state = $state

        let caja = this.API.service('alldist')

        caja.getList()
            .then((response) => {
                let dataSet = response.plain()

                this.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('data', dataSet)
                    //.withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()

                this.dtColumns = [
                    DTColumnBuilder.newColumn('user.id').withTitle('ID'),
                    DTColumnBuilder.newColumn('user.name').withTitle('Name'),
                    DTColumnBuilder.newColumn('user.email').withTitle('email'),
                    DTColumnBuilder.newColumn('user.username').withTitle('username'),
                    DTColumnBuilder.newColumn('proporcion').withTitle('proporcion').renderWith(function(data, type, full) {
                        return data + '%';
                    })
                    // DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    //.renderWith(actionsHtml)
                ]

                this.displayTable = true
            })
    }

    $onInit() {}
}

export const AllDistComponent = {
    templateUrl: './views/app/components/all-dist/all-dist.component.html',
    controller: AllDistController,
    controllerAs: 'vm',
    bindings: {}
};