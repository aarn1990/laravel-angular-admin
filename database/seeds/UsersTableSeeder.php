<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            0 =>['id' => 1,
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'email_verified' => '1',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()],
            1 =>['id' => 2,
            'name' => 'adrian',
            'username' => 'adrian',
            'email' => 'a@a.com',
            'password' => bcrypt('1'),
            'email_verified' => '1',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()]            
        ]);
    }
}
