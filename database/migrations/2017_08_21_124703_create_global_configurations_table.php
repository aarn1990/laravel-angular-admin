<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_configurations', function (Blueprint $table) {
            $table->increments('id');      
            $table->float('proporcion')->default(30);
            $table->integer('inicial_hour')->default(7);
            $table->integer('final_our')->default(20);
            $table->integer('min_apus')->default(100);
            $table->integer('credito')->default(250000);
            $table->integer('max_tickets_dia_eli')->default(5);
            $table->boolean('open')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('global_configurations');
    }
}
