<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caja_id')->unsigned()->index();
            $table->foreign('caja_id')->references('id')->on('cajas')->onDelete('cascade');
            $table->date('date');
            $table->integer('hour');           
            $table->boolean('canjeado');   
            $table->enum('state', ['1', '0', '2', '3'])->default(1);  
            $table->bigInteger("virtual_code");       
            $table->integer('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }
}
