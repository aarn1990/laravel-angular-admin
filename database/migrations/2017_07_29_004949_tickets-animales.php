<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketsAnimales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animales_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tickets_id')->unsigned()->index();
            $table->foreign('tickets_id')->references('id')->on('tickets')->onDelete('cascade');
            $table->integer('animales_id')->unsigned()->index();
            $table->foreign('animales_id')->references('id')->on('animales')->onDelete('cascade');
            $table->double('Value');
            $table->double('por_pagar');
            $table->boolean('ganador');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('animales_tickets');
    }
}
