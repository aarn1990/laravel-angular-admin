<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmacions', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto');
            $table->string('ip');
            $table->string('so');
            $table->integer('ganadors_id')->unsigned()->index();
            $table->foreign('ganadors_id')->references('id')->on('ganadors')->onDelete('cascade');
            $table->integer('tickets_id')->unsigned()->index();
            $table->foreign('tickets_id')->references('id')->on('tickets')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('confirmacions');
    }
}
