<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGanadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ganadors', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('hour');
            $table->float('pago');            
            $table->integer('animales_id')->unsigned()->index();
            $table->foreign('animales_id')->references('id')->on('animales')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ganadors');
    }
}
